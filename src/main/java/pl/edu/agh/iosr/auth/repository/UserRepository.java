package pl.edu.agh.iosr.auth.repository;

import pl.edu.agh.iosr.auth.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

}
