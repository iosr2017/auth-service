package pl.edu.agh.iosr.auth.service;

import pl.edu.agh.iosr.auth.domain.User;

public interface UserService {

	void create(User user);

}
